class Num{
    constructor(value){
        this.value = value;
    }

    isEvenOrOdd(){
        if(this.value % 2 == 0 ){
            console.log("even");
        }else{
            console.log("odd");
        }
    }
}

var num1 = new Num(6);
console.log(num1.isEvenOrOdd());